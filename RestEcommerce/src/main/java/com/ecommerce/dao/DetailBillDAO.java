package com.ecommerce.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.entity.DetailBill;

@Repository
public interface DetailBillDAO extends CrudRepository<DetailBill, Integer> {
	public List<DetailBill> findBybillID(int billID);
}
