package com.ecommerce.dao;

import org.springframework.data.repository.CrudRepository;

import com.ecommerce.entity.StateBill;

public interface StateBillDAO extends CrudRepository<StateBill, Integer>{
	public StateBill findById(int id);
}
