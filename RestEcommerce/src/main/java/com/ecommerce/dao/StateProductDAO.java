package com.ecommerce.dao;

import org.springframework.data.repository.CrudRepository;

import com.ecommerce.entity.StateProduct;


public interface StateProductDAO extends CrudRepository<StateProduct, Integer>{
	public StateProduct findById(int id);
}
