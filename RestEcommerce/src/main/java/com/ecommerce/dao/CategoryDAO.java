package com.ecommerce.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.entity.Category;

@Repository
public interface CategoryDAO extends CrudRepository<Category, Integer> {
	public List<Category> findAll();
	public Category findById(int Id);
}
