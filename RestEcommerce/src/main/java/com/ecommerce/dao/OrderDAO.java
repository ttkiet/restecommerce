package com.ecommerce.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.entity.Bill;
import com.ecommerce.entity.StateBill;

public interface OrderDAO extends CrudRepository<Bill, Integer> {
	public List<Bill> findByuserID(Integer userID);
	public Bill findByid(int iD);
	public Page<Bill> findAll(Pageable pageable);
}
