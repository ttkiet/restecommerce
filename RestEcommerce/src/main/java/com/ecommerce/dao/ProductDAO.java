package com.ecommerce.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ecommerce.entity.Product;

@Repository
public interface ProductDAO extends CrudRepository<Product, Integer>{
	public Page<Product> findByCategory(int category, Pageable pageable);
	
	@Query("SELECT pd FROM Product pd WHERE ((pd.category = :categoryid OR :categoryid = -1) AND pd.name like %:nameSearch%)")
	public Page<Product> findByCategoryAndName( @Param("categoryid") int categoryid, @Param("nameSearch")String nameSearch,  Pageable pageable);

	public Page<Product> findAll(Pageable pageable);
	public Product findById(int id);
}
