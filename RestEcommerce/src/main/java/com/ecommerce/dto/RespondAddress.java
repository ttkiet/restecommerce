package com.ecommerce.dto;

public class RespondAddress {
	private int id;
	private String personReceive;
	private String address;
	private String phoneNumber;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPersonReceive() {
		return personReceive;
	}

	public void setPersonReceive(String personReceive) {
		this.personReceive = personReceive;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public RespondAddress() {

	}

	public RespondAddress(com.ecommerce.entity.Address address) {
		this.id = address.getiD();
		this.address= address.getAddress();
		this.personReceive = address.getPersonReceive();
		this.phoneNumber = address.getPhoneNumber();
	}
}
