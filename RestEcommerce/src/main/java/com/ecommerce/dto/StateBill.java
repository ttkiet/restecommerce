package com.ecommerce.dto;

public class StateBill {
	private int id;
	private String state;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String name) {
		this.state = name;
	}
	public StateBill() {
	}
	public StateBill(com.ecommerce.entity.StateBill statebill) {
		this.id = statebill.getId();
		this.state = statebill.getState();
	}
	
}
