package com.ecommerce.dto;

import java.util.Date;


public class Bill {
	private int id;
	private Integer userID;
	private Date date;
	private Float total;
    private Address address;
    private StateBill state;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Float getTotal() {
		return total;
	}
	public void setTotal(Float total) {
		this.total = total;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public StateBill getState() {
		return state;
	}
	public void setState(StateBill state) {
		this.state = state;
	}
	public Bill() {
	}
    
	public Bill(com.ecommerce.entity.Bill bill) {
		this.id = bill.getId();
		this.userID = bill.getUserID();
		this.date = bill.getDate();
		this.total =bill.getTotal();
		this.address = new Address(bill.getAddress());
		this.state = new StateBill(bill.getState());
	}
}
