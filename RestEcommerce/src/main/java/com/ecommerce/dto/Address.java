package com.ecommerce.dto;

import javax.persistence.Column;

public class Address {
	private int id;
	private String personReceive;
	private String address;
	private String phoneNumber;
	private int userID;
	private int isDelete;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPersonReceive() {
		return personReceive;
	}
	public void setPersonReceive(String personReceive) {
		this.personReceive = personReceive;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	public Address() {
	}
	public Address(com.ecommerce.entity.Address address) {
		this.id = address.getiD();
		this.personReceive = address.getPersonReceive();
		this.phoneNumber = address.getPhoneNumber();
		this.address = address.getAddress();
		this.userID = address.getUserID();
		this.isDelete = address.getIsDelete();
	}
	
}
