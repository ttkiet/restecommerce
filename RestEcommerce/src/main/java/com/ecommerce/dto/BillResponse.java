package com.ecommerce.dto;

import java.util.Date;

public class BillResponse {
	private int id;
	private Date date;
	private Float total;
    private String address;
    private String state;
    public BillResponse(com.ecommerce.entity.Bill bill) {
		this.id = bill.getId();
		this.date = bill.getDate();
		this.total =bill.getTotal();
		this.address = bill.getAddress().getAddress();
		this.state = bill.getState().getState();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Float getTotal() {
		return total;
	}
	public void setTotal(Float total) {
		this.total = total;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
    
}
