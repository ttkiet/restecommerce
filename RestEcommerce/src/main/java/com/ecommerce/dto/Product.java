package com.ecommerce.dto;

public class Product {

	private int id;
	private String name;
	private String description;
	private float price;
	private int category;
	private String image;
	private StateProduct state;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public StateProduct getState() {
		return state;
	}

	public void setState(StateProduct state) {
		this.state = state;
	}

	public Product(int id, String name, String description, float price, int category, String image,
			StateProduct state) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.category = category;
		this.image = image;
		this.state = state;
	}

	public Product(com.ecommerce.entity.Product product) {
		this.id = product.getId();
		this.name = product.getName();
		this.description = product.getDescription();
		this.price = product.getPrice();
		this.category = product.getCategory();
		this.image = product.getImage();
		this.state = new com.ecommerce.dto.StateProduct(product.getState());
	}

}
