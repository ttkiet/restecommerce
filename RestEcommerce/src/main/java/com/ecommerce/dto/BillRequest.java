package com.ecommerce.dto;

import java.util.List;

public class BillRequest {
	private int userId;
	private Float total;
	private int addressId;
	private List<DetailBillRequest> detail;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Float getTotal() {
		return total;
	}
	public void setTotal(Float total) {
		this.total = total;
	}
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public List<DetailBillRequest> getDetail() {
		return detail;
	}
	public void setDetail(List<DetailBillRequest> detail) {
		this.detail = detail;
	}
	
}
