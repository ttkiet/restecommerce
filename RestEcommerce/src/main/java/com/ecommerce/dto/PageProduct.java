package com.ecommerce.dto;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

public class PageProduct {
	private List<Product> products;
	private int totalPages;
	private int size;
	private int currentPage;
	
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public PageProduct(List<Product> content, int totalPages, int size) {
		this.products = content;
		this.totalPages = totalPages;
		this.size = size;
	}
	public PageProduct() {
	}
	public PageProduct( Page<com.ecommerce.entity.Product> products, int currentPage) {
		this.products = products.getContent().stream().map(jpaproduct->new Product(jpaproduct)).collect(Collectors.toList());
		this.totalPages = products.getTotalPages();
		this.size = products.getSize();
		this.currentPage = currentPage;
	}
	
	
}
