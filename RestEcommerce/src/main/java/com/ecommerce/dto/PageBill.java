package com.ecommerce.dto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

public class PageBill {
	private List<BillResponse> bills;
	private int totalPages;
	private int size;
	private int currentPage;
	public PageBill() {
	}
	public PageBill( Page<com.ecommerce.entity.Bill> bills, int currentPage) {
		this.bills = bills.getContent().stream().map(bill->new BillResponse(bill)).collect(Collectors.toList());
		this.totalPages = bills.getTotalPages();
		this.size = bills.getSize();
		this.currentPage = currentPage;
	}
	
	public List<BillResponse> getBills() {
		return bills;
	}
	public void setBills(List<BillResponse> bills) {
		this.bills = bills;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	
}
