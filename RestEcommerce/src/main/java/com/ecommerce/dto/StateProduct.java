package com.ecommerce.dto;


public class StateProduct {
	private int id;
	private String state;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public StateProduct() {
	}
	public StateProduct(int id, String state) {
		this.id = id;
		this.state = state;
	}
	public StateProduct(com.ecommerce.entity.StateProduct state2) {
		this.id = state2.getId();
		this.state = state2.getState();
	}
	
}
