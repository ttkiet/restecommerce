package com.ecommerce.dto;

public class BillUpdate {
	private int billId;
	private int stateId;
	public int getBillId() {
		return billId;
	}
	public void setBillId(int billId) {
		this.billId = billId;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public BillUpdate() {
	}
	public BillUpdate(int billId, int stateId) {
		this.billId = billId;
		this.stateId = stateId;
	}
	
}
