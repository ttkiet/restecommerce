package com.ecommerce.dto;

public class Paging {
	private int page;
	private int pagesize;
	private String sort;
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public Paging() {
	}
	public Paging(int page, int pagesize, String sort) {
		this.page = page;
		this.pagesize = pagesize;
		this.sort = sort;
	}
	
}
