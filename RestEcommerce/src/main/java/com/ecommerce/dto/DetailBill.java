package com.ecommerce.dto;

public class DetailBill {
	private int orderId;
	private String productName;
	private Float priceProduct;
	private Integer amount;
	public DetailBill(com.ecommerce.entity.DetailBill detailbill) {
		this.orderId = detailbill.getBillID();
		this.productName = detailbill.getProduct().getName();
		this.priceProduct = detailbill.getProduct().getPrice();
		this.amount = detailbill.getAmount();
	}
	
	
}
