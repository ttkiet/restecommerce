package com.ecommerce.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.dao.StateBillDAO;
import com.ecommerce.entity.StateBill;

@Service
public class StateBillService {
	@Autowired
	StateBillDAO statebilldao;
	public StateBill findById(int id) {
		return this.statebilldao.findById(id);
	}
}
