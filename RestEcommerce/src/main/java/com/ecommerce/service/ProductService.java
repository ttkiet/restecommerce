package com.ecommerce.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.ecommerce.DefineDefaultValue;
import com.ecommerce.dao.ProductDAO;
import com.ecommerce.entity.Product;

import javassist.NotFoundException;

@Service
public class ProductService {
	@Autowired
	ProductDAO productdao;

	public Page<Product> findAllProduct(int pageNumber, int pageSize, String sort) {
		if (pageNumber < 1) {
			pageNumber = DefineDefaultValue.PAGE_DEFAULT;
		}
		if (pageSize < 0) {
			pageSize = DefineDefaultValue.PAGE_SIZE_DEFAULT;
		}

		Pageable pageable = getPageable(pageNumber, pageSize, sort);
		Page<Product> product = productdao.findAll(pageable);
		return product;
	}

	public Product findById(int idProduct) {
		return this.productdao.findById(idProduct);
	}

	public Product save(Product newProduct) {
		this.productdao.save(newProduct);
		return newProduct;
	}

	public void delete(int idproduct) throws NotFoundException {
		Product resultProduct = this.productdao.findById(idproduct);
		if (resultProduct == null) {
			throw new NotFoundException("Product not exist");
		} else {
			this.productdao.deleteById(idproduct);
		}
	}
	
	public Product update(Product productUpdate) {
		return this.productdao.save(productUpdate);
	}

	public Page<Product> findProductByCategory(int pageNumber, int pageSize, String sort, int idCategory){
		if (pageNumber < 1) {
			pageNumber = DefineDefaultValue.PAGE_DEFAULT;
		}
		if (pageSize < 0) {
			pageSize = DefineDefaultValue.PAGE_SIZE_DEFAULT;
		}

		Pageable pageable = getPageable(pageNumber, pageSize, sort);
		Page<Product> result =  this.productdao.findByCategory(idCategory, pageable);
		return result;
	}
	public Page<Product> seachProduct(int pageNumber, int pageSize, String sort, int idCategory, String name){
		Page<>
		return null;
	}
	private Sort getSort(String condition) {
		if (condition.toLowerCase().contains("desc")) {
			return Sort.by(Direction.DESC, "name");
		} else {
			return Sort.by(Direction.ASC, "name");
		}
	}

	private Pageable getPageable(int pageNumber, int pageSize, String s_sort) {
		if (s_sort.equals("")) {
			return PageRequest.of(pageNumber - 1, pageSize);
		} else {
			Sort sort = this.getSort(s_sort);
			return PageRequest.of(pageNumber - 1, pageSize, sort);
		}

	}

}
