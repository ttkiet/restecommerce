package com.ecommerce.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ecommerce.DefineDefaultValue;
import com.ecommerce.dao.DetailBillDAO;
import com.ecommerce.dao.OrderDAO;
import com.ecommerce.dao.StateBillDAO;
import com.ecommerce.dto.BillRequest;
import com.ecommerce.dto.BillUpdate;
import com.ecommerce.dto.DetailBillRequest;
import com.ecommerce.entity.Bill;
import com.ecommerce.entity.StateBill;

@Service
public class BillService {
	@Autowired
	OrderDAO billdao;
	@Autowired
	DetailBillDAO detailbilldao;
	@Autowired
	StateBillDAO statebilldao;
	public Page<Bill> findAllBill(int page, int pagesize) {
		if(page < 1) {
			page = DefineDefaultValue.PAGE_DEFAULT;
		}
		if(pagesize < 1) {
			page = DefineDefaultValue.PAGE_SIZE_DEFAULT;
		}
		Pageable pageable = PageRequest.of(page - 1, pagesize);
		Page<Bill> bill = this.billdao.findAll(pageable);
		return bill;
	}
	
	public Bill findById(int idBill) {
		Bill bill = this.billdao.findByid(idBill);
		return bill;
	}
	
	public List<Bill> findByuserId(int idUser) {
		List<Bill> bill = this.billdao.findByuserID(idUser);
		return bill;
	}

	public Bill saveBill(BillRequest cart) {
		Bill newBill = new Bill(cart.getUserId(), new Date(), cart.getTotal(), cart.getAddressId(), DefineDefaultValue.STATE_BILL_CREATE);
		this.billdao.save(newBill);
		return newBill;
	}
	
	public Bill updateBill(int idBill, int idState) {
		Bill bill = this.billdao.findByid(idBill);
		StateBill newState = this.statebilldao.findById(idState);
		bill.setState(newState);
		this.billdao.save(bill);
		return bill;
	}

	
}
