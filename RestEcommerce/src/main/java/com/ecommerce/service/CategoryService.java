package com.ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.dao.CategoryDAO;
import com.ecommerce.entity.Category;

@Service
public class CategoryService {
	@Autowired
	CategoryDAO categorydao;
	public List<Category> findAllCategory(){
		return this.categorydao.findAll();
	}
	public Category addCategory(Category newCategory) {
		this.categorydao.save(newCategory);
		return newCategory;
	}
	public Category updateCategory(Category categoryUpdate) {
		this.categorydao.save(categoryUpdate);
		return categoryUpdate;
	}
	
}
