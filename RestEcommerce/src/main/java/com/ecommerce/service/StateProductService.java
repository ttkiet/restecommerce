package com.ecommerce.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.dao.StateProductDAO;
import com.ecommerce.entity.StateProduct;

@Service
public class StateProductService {
	@Autowired
	StateProductDAO stateproductdao;
	public StateProduct findById(int id) {
		return this.stateproductdao.findById(id);
	}
}
