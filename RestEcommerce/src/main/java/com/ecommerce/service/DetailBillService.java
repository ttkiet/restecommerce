package com.ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.dao.DetailBillDAO;
import com.ecommerce.dto.DetailBillRequest;
import com.ecommerce.entity.DetailBill;

@Service
public class DetailBillService {
	@Autowired
	DetailBillDAO detailbilldao;
	
	public void saveDetailBill(List<DetailBillRequest> details, int idBill) {
		for (DetailBillRequest item : details) {
			DetailBill detail = new DetailBill(item.getAmount(), idBill, item.getProductId());
			this.detailbilldao.save(detail);
		}
	}
}
