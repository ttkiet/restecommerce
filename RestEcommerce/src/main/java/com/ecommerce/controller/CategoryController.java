package com.ecommerce.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.entity.Category;
import com.ecommerce.service.CategoryService;

@RestController
@RequestMapping(value = {"/category"})
public class CategoryController {
	@Autowired
	CategoryService categoryservice;
	
	@RequestMapping(value = {""}, method =  RequestMethod.GET)
	public List<com.ecommerce.dto.Category> getAllCategory(){
		List<Category> categories = this.categoryservice.findAllCategory();
		return categories.stream().map(category-> new com.ecommerce.dto.Category(category)).collect(Collectors.toList());
	}
	
	@RequestMapping(value = {""}, method =  RequestMethod.POST)
	public com.ecommerce.dto.Category addCategory(@RequestBody com.ecommerce.dto.Category category){
		Category newCategory = new Category(category);
		com.ecommerce.dto.Category responseCategory = new com.ecommerce.dto.Category(this.categoryservice.addCategory(newCategory));
		return responseCategory;
	}
	
	@RequestMapping(value = {"/{idcategory}"}, method =  RequestMethod.PUT)
	public com.ecommerce.dto.Category updateCategory(@RequestBody String category, @PathVariable int idcategory){
		Category newCategory = new Category(idcategory, category);
		com.ecommerce.dto.Category responseCategory = new com.ecommerce.dto.Category(this.categoryservice.updateCategory(newCategory));
		return responseCategory;
	}
	
}
