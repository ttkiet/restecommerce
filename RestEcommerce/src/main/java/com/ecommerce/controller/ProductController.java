package com.ecommerce.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.dto.PageProduct;
import com.ecommerce.entity.Product;
import com.ecommerce.service.ProductService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = {"/product"})
public class ProductController {
	@Autowired
	ProductService productservice;
	ModelMapper modelMapper = new ModelMapper();
	
	@RequestMapping(value= {""}, method =  RequestMethod.GET)
	public com.ecommerce.dto.PageProduct getAllProductAdmin(@RequestParam(defaultValue = "1")  int page 
									, @RequestParam(defaultValue = "10") int pagesize
									, @RequestParam(defaultValue = "") String sort){
		Page<Product> productPage = productservice.findAllProduct(page, pagesize, sort);
		
		return new PageProduct(productPage, page);
	}
	
	@RequestMapping(value= {"/{idproduct}"}, method =  RequestMethod.GET)
	public com.ecommerce.dto.Product getProductById(@PathVariable int idproduct){
		Product product = productservice.findById(idproduct);
		return new com.ecommerce.dto.Product(product);
	}
	

	@RequestMapping(value= {"/"}, method =  RequestMethod.POST)
	public com.ecommerce.dto.Product addProduct(@RequestBody com.ecommerce.dto.Product product){
		Product newProduct =  new Product(product);
		com.ecommerce.dto.Product result = new com.ecommerce.dto.Product(this.productservice.save(newProduct));
		return result;
	}
	
	@RequestMapping(value= {"/{idproduct}"}, method =  RequestMethod.PUT)
	public  com.ecommerce.dto.Product updateProduct(@RequestBody com.ecommerce.dto.Product product){
		Product newProduct =  new Product(product);
		com.ecommerce.dto.Product result = new com.ecommerce.dto.Product(this.productservice.update(newProduct));
		return result;
	}
	
	@RequestMapping(value= {"/{idproduct}"}, method =  RequestMethod.DELETE)
	public void deleteProduct(@PathVariable int idproduct) throws NotFoundException{
		this.productservice.delete(idproduct);
	}
	
	@RequestMapping(value= {"/category/{idcategory}"}, method =  RequestMethod.GET)
	public PageProduct getProductBycategory( @PathVariable int idcategory
									, @RequestParam(defaultValue = "1")  int page 
									, @RequestParam(defaultValue = "10") int pagesize
									, @RequestParam(defaultValue = "") String sort){
		Page<Product> result = this.productservice.findProductByCategory(page, pagesize, sort, idcategory);
		return new PageProduct(result, page);
	}
	@RequestMapping(value= {"/search/{idcategory}"}, method =  RequestMethod.GET)
	public PageProduct searchProduct( @PathVariable int idcategory
									, @RequestParam(defaultValue = "") String name
									, @RequestParam(defaultValue = "1")  int page 
									, @RequestParam(defaultValue = "10") int pagesize
									, @RequestParam(defaultValue = "") String sort){
		Page<Product> result = this.productservice.findProductByCategory(page, pagesize, sort, idcategory);
		return new PageProduct(result, page);
	}
}
