package com.ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.dto.BillRequest;
import com.ecommerce.dto.BillUpdate;
import com.ecommerce.dto.PageBill;
import com.ecommerce.entity.Bill;
import com.ecommerce.service.BillService;
import com.ecommerce.service.DetailBillService;

@RestController
@RequestMapping(value = { "/order" })
public class OrderController {
	@Autowired
	BillService orderservice;
	@Autowired
	DetailBillService detailbillservice;

	@RequestMapping(value = { "" }, method = RequestMethod.GET)
	public PageBill getAllBill(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int pagesize) {
		Page<Bill> bills = this.orderservice.findAllBill(page, pagesize);
		return new PageBill(bills, page);
	}

	@RequestMapping(value = { "" }, method = RequestMethod.POST)
	public com.ecommerce.dto.Bill addBill(@RequestBody BillRequest cart) {
		Bill newBill = this.orderservice.saveBill(cart);
		this.detailbillservice.saveDetailBill(cart.getDetail(), newBill.getId());
		return new com.ecommerce.dto.Bill(newBill);
	}

	@RequestMapping(value = { "/{idBill}" }, method = RequestMethod.PUT)
	public com.ecommerce.dto.Bill updateStateBill(@RequestBody int idState, @PathVariable int idBill) {
		Bill billAfterUpdate = this.orderservice.updateBill(idBill, idState);
		return new com.ecommerce.dto.Bill(billAfterUpdate);
	}
}
