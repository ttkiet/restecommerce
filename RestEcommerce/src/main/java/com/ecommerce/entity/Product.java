package com.ecommerce.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product implements Serializable {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "nameproduct")
	private String name;

	@Column(name = "descriptionproduct")
	private String description;

	@Column(name = "Price")
	private float price;

	@Column(name = "id_category")
	private int category;

	@Column(name = "image")
	private String image;
	
	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_State")
    private StateProduct state;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public StateProduct getState() {
		return state;
	}

	public void setState(StateProduct state) {
		this.state = state;
	}

	public Product(int id, String name, String description, float price, int category, String image,
			StateProduct state) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.category = category;
		this.image = image;
		this.state = state;
	}

	public Product() {
	}

	public Product(com.ecommerce.dto.Product product) {
		this.id = product.getId();
		this.name = product.getName();
		this.description = product.getDescription();
		this.price = product.getPrice();
		this.category = product.getCategory();
		this.image = product.getImage();
		this.state = new StateProduct(product.getState());
	}

	public Product(int id_product) {
		this.id = id_product;
	}
	
	
	
}
