package com.ecommerce.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "statebill")
public class StateBill {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "state")
	private String state;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public StateBill(int id, String state) {
		this.id = id;
		this.state = state;
	}
	public StateBill() {
	}
	
	public StateBill(com.ecommerce.dto.StateBill statebill) {
		this.id = statebill.getId();
		this.state = statebill.getState();
	}
	public StateBill(int id) {
		this.id = id;
	}
	
	
}
