package com.ecommerce.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Place_Shipping")
public class Address {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iD;
	
	@Column(name = "personreceive")
	private String personReceive;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "phone")
	private String phoneNumber;
	
	@Column(name = "userid")
	private int userID;
	
	@Column(name = "isdelete")
	private int isDelete;

	public int getiD() {
		return iD;
	}

	public void setiD(int iD) {
		this.iD = iD;
	}

	public String getPersonReceive() {
		return personReceive;
	}

	public void setPersonReceive(String personReceive) {
		this.personReceive = personReceive;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public Address(int iD, String personReceive, String address, String phoneNumber, int userID, int isDelete) {
		this.iD = iD;
		this.personReceive = personReceive;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.userID = userID;
		this.isDelete = isDelete;
	}

	public Address() {
	}
	
	public Address(com.ecommerce.dto.Address address) {
		this.iD = address.getId();
		this.personReceive = address.getPersonReceive();
		this.phoneNumber = address.getPhoneNumber();
		this.address = address.getAddress();
		this.userID = address.getUserID();
		this.isDelete = address.getIsDelete();
	}

	public Address(int iD) {
		this.iD = iD;
	}
	
	
}
