package com.ecommerce.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ecommerce.service.BillService;

@Entity
@Table(name = "bill")
public class Bill {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "id_user")
	private Integer userID;
	
	@Column(name = "datebuy")
	private Date date;
	
	@Column(name = "total")
	private Float total;
	

	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_placeshipping")
    private Address address;

	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_State")
    private StateBill state;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public StateBill getState() {
		return state;
	}

	public void setState(StateBill state) {
		this.state = state;
	}

	public Bill() {
	}

	public Bill(com.ecommerce.dto.Bill bill) {
		this.id = bill.getId();
		this.userID = bill.getUserID();
		this.date = bill.getDate();
		this.total =bill.getTotal();
		this.address = new Address(bill.getAddress());
		this.state = new StateBill(bill.getState());
	}
	
	public Bill(int id, Integer userID, Date date, Float total, Address address, StateBill state) {
		this.id = id;
		this.userID = userID;
		this.date = date;
		this.total = total;
		this.address = address;
		this.state = state;
	}
	public Bill(Integer userID, Date date, Float total, int address, int state) {
		this.userID = userID;
		this.date = date;
		this.total = total;
		this.address = new Address(address);
		this.state = new StateBill(state);
	}
	public Bill(int id, int stateid) {
		this.id = id;
		this.state = new StateBill(stateid);
	}
	
}
