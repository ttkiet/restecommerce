package com.ecommerce.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stateproduct")
public class StateProduct {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "state")
	private String state;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public StateProduct(int id, String state) {
		this.id = id;
		this.state = state;
	}
	public StateProduct() {
	}
	public StateProduct(com.ecommerce.dto.StateProduct state2) {
		this.state = state2.getState();
		this.id = state2.getId();
	}
	
	
}
